<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scele=1.0">
        <title>Portofolio Reza Sefiyanti</title>
        <link rel="stylesheet" type="text/css" href="pesan.css">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    </head>
    <head>
        <header>
            <a href="home.html" class="logo">Yuk Sampaikan Pesan kamu disini</a>
            <div class="menu-toggle"></div>
            <nav>
                <ul>
                    <li><a href="home.html" class="">Home</a></li>
                    <li><a href="http://localhost/PTA-Pweb2022-I-2100018429-RezaSefiyanti/Location.php" class="">Location</a></li>
                    <li><a href="galerry.html" class="">Galerry</a></li>
                    <li><a href="http://localhost/PTA-Pweb2022-I-2100018429-RezaSefiyanti/message.php" class="active">Message for me</a></li>

                </ul>
            </nav>
            <div class="clearfix"></div>
        </header>
        <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $(".menu-toggle").click(function(){
                    $(".menu-toggle").toggleClass("active")
                    $("nav").toggleClass("active")
                })
            })
        </script>
    </head>
    <body>
        <div class="wrap" style="color: #a37f30">
            <div class="container">
                <h1>Tulis dibawah ini</h1>
                <form name="form1" method="post" action="proses.php" onSubmit="validasi()">
                    <table>
                        <tr>
                            <td>Nama Lengkap</td>
                            <td>:</td>
                            <td><input type="text" name="nama_lengkap"></td>
                        </tr>    
                        <tr>
                            <td>Provinsi</td>
                            <td>:</td>
                            <td>
                                <select name="Provinsi">
                                    <option>--Pilih Provinsi--</option>
                                    <option>Aceh</option>
                                    <option>Sumatra Utara</option>
                                    <option>Sumatra Barat</option>
                                    <option>Riau</option>
                                    <option>Kepulauan Riau</option>
                                    <option>Jambi</option>
                                    <option>Bengkulu</option>
                                    <option>Sumatra Selatan</option>
                                    <option>Kepulauan Bangka Belitung</option>
                                    <option>Lampung</option>
                                    <option>Banten</option>
                                    <option>Jawa Barat</option>
                                    <option>Jakarta</option>
                                    <option>Jawa Tengah</option>
                                    <option>Yogyakarta</option>
                                    <option>Jawa Timur</option>
                                    <option>Bali</option>
                                    <option>Nusa Tenggara Barat</option>                                        
                                    <option>Nusa Tenggara Timur</option>
                                    <option>Kalimantan Barat</option>
                                    <option>Kalimantan Selatan</option>
                                    <option>Kalimantan Tengah</option>
                                    <option>Kalimantan Timur</option>
                                    <option>Kalimantan Utara</option>
                                    <option>Gorontalo</option>
                                    <option>Sulawsi Barat</option>
                                    <option>Sulawsi Selatan</option>
                                    <option>Sulawsi Tengah</option>
                                    <option>Sulawsi Tenggara</option>
                                    <option>Sulawsi Utara</option>
                                    <option>Maluku</option>
                                    <option>Maluku Utara</option>
                                    <option>Papua Barat</option>
                                    <option>Papua</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td>:</td>
                            <td><textarea name="Alamat"></textarea></td>
                        </tr>
                        <tr>
                            <td>No Telp</td>
                            <td>:</td>
                            <td><input type="number" name="telp"></td>
                        </tr>         
                        <tr>
                            <td>Jenis Kelamin</td>
                            <td>:</td>
                            <td>
                                <input type="radio" name="jkelamin" value="Laki-Laki">Laki-Laki
                                <input type="radio" name="jkelamin" value="Perempuan">Perempuan
                            </td>
                        </tr>
                        <tr>
                            <td>Pesan</td>
                            <td>:</td>
                            <td><textarea name="pesan"></textarea>
                            </td>
                        </tr>    
                <tr>
                    <td></td>
                    <td></td>
                    <td>
                        <button type="submit" name="kirim">kirim</button>
                        <button><a link href="home.html">kembali</a></button>
                    </td>
                </tr>
                </table>
                </form>
            </div>
        </div>
        <div class="footertop">
                <div class="medsos">
                    <ul>
                        <li><a href="https://www.facebook.com/ezha.sefiyanty"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="http://instagram.com/reza_sfxyzz"><i class="fab fa-instagram"></i></a></li>
                        <li><a href="https://wa.me/qr/MX47IS6KYAVXD1"><i class="fab fa-whatsapp"></i></a></li>
                    </ul>  
                </div>                         
            </div>
            <div class="footerbottom">
                <div class="copyright">
                    <p class="footer-text">Copyright 2022<span class="span-text"> Reza Sefiyanti</span> All right reserved</p>
                </div>
            </div>
        </div>  
    </body>


</html>